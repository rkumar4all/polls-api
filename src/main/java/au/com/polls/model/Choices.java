package au.com.polls.model;

/**
 * @author rkumar
 *
 */
public class Choices {

	private long votes;
	private String choice;

	/**
	 * @return
	 */
	public long getVotes() {
		return votes;
	}

	/**
	 * @param votes
	 */
	public void setVotes(long votes) {
		this.votes = votes;
	}

	/**
	 * @return
	 */
	public String getChoice() {
		return choice;
	}

	/**
	 * @param choice
	 */
	public void setChoice(String choice) {
		this.choice = choice;
	}

}
