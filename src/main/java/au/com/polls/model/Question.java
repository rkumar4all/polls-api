package au.com.polls.model;

import java.util.List;

/**
 * @author rkumar
 *
 */
public class Question {

	private String question;
	private String published_at;
	private String url;
	private List<Choices> choices;

	/**
	 * @return
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * @param question
	 */
	public void setQuestion(String question) {
		this.question = question;
	}

	/**
	 * @return
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return
	 */
	public String getPublished_at() {
		return published_at;
	}

	/**
	 * @param published_at
	 */
	public void setPublished_at(String published_at) {
		this.published_at = published_at;
	}

	/**
	 * @return
	 */
	public List<Choices> getChoices() {
		return choices;
	}

	/**
	 * @param choices
	 */
	public void setChoices(List<Choices> choices) {
		this.choices = choices;
	}

}
