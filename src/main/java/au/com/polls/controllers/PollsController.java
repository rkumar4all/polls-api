package au.com.polls.controllers;

import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.polls.model.Question;

/**
 * @author rkumar
 *
 */
@RestController
public class PollsController {

	/**
	 * @param question
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/polls-api/getQuestion")
	public String getQuestion(@RequestParam(value = "question", defaultValue = "1") String question) throws Exception {
		return getQuestionDetailsForId(question);
	}

	/**
	 * @param questionNo
	 * @return
	 * @throws Exception
	 */
	private String getQuestionDetailsForId(String questionNo) throws Exception {

		JSONArray jsonData = readJsonData();

		System.out.println("questionNo=" + questionNo);//TODO - replace later with logger
		return searchQuestion(jsonData.toString(), "/questions/" + questionNo);

	}

	/**
	 * @return
	 */
	@RequestMapping("/polls-api/getall")
	public JSONArray getAllQuestions() {
		return readJsonData();
	}

	/**
	 * @return
	 */
	private JSONArray readJsonData() {
		JSONParser parser = new JSONParser();
		JSONArray jsonArray = null;
		/**
		 * Currently copy data.json under C drive.
		 */
		String content = "C:\\polls-api\\resources\\data.json";// TODO
		try {
			Object obj = parser.parse(new FileReader(content));

			jsonArray = (JSONArray) obj;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonArray;
	}

	/**
	 * @param jsonString
	 * @param url
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	private String searchQuestion(String jsonString, String url) throws IOException, Exception {

		Question[] questions = new ObjectMapper().readValue(jsonString, Question[].class);
		for (Question question : questions) {
			if (question.getUrl().equals(url)) {
				return new ObjectMapper().writeValueAsString(question).toString();
			}
		}
		return "result not found";//TODO
	}

}
